## This web service was developed using Spring Boot and TensorFlow. 

It accepts an image on the */analyze* POST endpoint and a required request part 'file', it returns a JSON array with some characteristics extracted such as:

* objects detected

* number of people detected

* main color detected

* main color detected (hue)

* if the image contains transparency

* size of the image (width and height)

* the image in base64

* name of the file

* extension of the file sent *(if is not an image extension it will return a message to advice it)*

The app for testing is available on Heroku at https://imganalyzer.herokuapp.com/

*The images can not be larger than 5MB*